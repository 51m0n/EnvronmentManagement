module 51m0n.com/EnvironmentManagerAPI/v2

go 1.15

require (
	github.com/coocood/freecache v1.1.1
	github.com/gin-gonic/gin v1.7.4
	go.mongodb.org/mongo-driver v1.7.2
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
