package main

import (
	"os"

	"51m0n.com/EnvironmentManagerAPI/v2/src"
	"github.com/gin-gonic/gin"
)

func main() {
	src.DbConnect()
	r := gin.Default()
	r.Use(gin.Recovery())
	initializeRoutes(r)
	r.Run(getPort())
	src.DbDisconnect()
}

func initializeRoutes(r *gin.Engine) {
	r.POST("/data", src.CreateData)
	r.PUT("/data/:id", src.CreateEnvironmentData)
	r.Use(src.AEADHandler)
	{
		r.PUT("/data/authed/:id", src.CreateEnvironmentData)
	}
}

func getPort() string {
	port := os.Getenv("GIN_PORT")

	if port == "" {
		port = "8080"
	}

	return ":" + port
}
