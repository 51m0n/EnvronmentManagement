#pragma once
#include <BME280I2C.h>
#include <Ticker.h>
#include <U8g2lib.h>
#include <ESP8266HTTPClient.h>
#include "byte_buffer.h"

//scl D1
//sda D2
U8G2_SSD1306_128X32_WINSTAR_1_SW_I2C screen(U8G2_R0, PIN_WIRE_SCL, PIN_WIRE_SDA);
BME280I2C bme;
Ticker environmentUpdate;
Ticker environmentPost;
HTTPClient requests;
WiFiClient client;

BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
BME280::PresUnit presUnit(BME280::PresUnit_Pa);

float pressure = 0;
float temp = 0;
float humidity = 0;

#define LINE_1 11
#define LINE_2 23
#define FONT u8g2_font_helvR10_te

#define TEMP "Temperature: "
#define HUMID "Humidity: "
    
#define DERIVATION_HASH_SIZE 32
static constexpr const char *DERIVATION_SALT = "ENVIRONMENT";

void UpdateEnvironment();

void PostEnvironment();

void InitializeWifi();

utils::byte_buffer Encrypt(std::string data);

void DeriveKey();