#pragma once
#include <cstdint>
#include <string>

namespace Environment
{
    class Settings
    {
    private:
        /* data */
    public:
        uint64_t apiID;

        std::string endpoint;

        std::string passcode;
    };

}