#pragma once
#include <ESP8266WebServer.h>
#include <map>
#include <utils.h>

class setup_server
{
    enum class SetupErrorCodes
    {
        SUCCESS = 0,
        CONNECTION_ERROR
    };

  public:
    #define AP_SSID "Test_AP"
    #define AP_PASSWORD "pass-to-soft-AP"

    setup_server();

    ~setup_server();

    SetupErrorCodes get_connection(const std::string &ssid, const std::string &password);

  private:
    ESP8266WebServer *server;

    bool serverConnection;

    uint8_t connect_to_ap(const std::string &ssid, const std::string &password);

    void root_callback();

    void ap_submission_callback();

    void not_found_callback();
};