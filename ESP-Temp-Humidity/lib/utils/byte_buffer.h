#pragma once
#include <stdint.h>
#include <string>

namespace utils
{
class byte_buffer
{

public:
    byte_buffer(uint_fast16_t length);
    ~byte_buffer();

    const bool is_valid();

    unsigned char *get_ptr();
    const uint_fast16_t get_length();

    unsigned char *get_ptr(const uint_fast16_t index, uint_fast16_t &length);

    const bool copy_to(void *destinationBuffer, uint_fast16_t &index, const uint_fast16_t &length);
    const bool copy_from(const void *const sourceBuffer, uint_fast16_t &index, const uint_fast16_t &length);      

    const bool clone(byte_buffer &bufferToClone);

    const void clear_buffer();
    const void fill_random();

    const uint_fast16_t load_base64(std::string base64Data);
    const std::string get_base64();
    const std::string to_string();

private:
    unsigned char *_buffer;
    const uint_fast16_t _length;
};

} // namespace Control_System