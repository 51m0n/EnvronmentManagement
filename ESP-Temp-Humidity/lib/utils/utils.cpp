#include <utils.h>
#include <stdio.h>

#ifdef UNIT_TEST
    #define PRINTLN(x)
    #define PRINT(x)
#else
    #define PRINTLN(x) Serial.println(x)
    #define PRINT(x) Serial.print(x)
#endif

namespace utils
{

void debug_print(const char *print)
{
    PRINTLN(print);
}

void debug_print(const std::vector<byte> print)
{
    for (const auto &byte : print)
    {
        PRINT(byte);
    }
    PRINT('\n');
}

void debug_print(String print)
{
    PRINTLN(print);
}

void debug_print(std::string print)
{
    PRINTLN(print.c_str());
}

void debug_print(long print)
{
    debug_print(to_string(print));
}

std::string to_string(long n)
{
    char buf[15];
    sprintf(buf, "%li", n);
    return std::string(buf);
}

} // namespace utils