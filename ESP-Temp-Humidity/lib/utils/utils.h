#pragma once
#include <map>
#include <Arduino.h>
#include <string>

namespace utils
{

void debug_print(const char* print);
void debug_print(const std::vector<byte> print);
void debug_print(String print);
void debug_print(std::string print);
void debug_print(long print);

std::string to_string(long n);
}