﻿using MongoDB.Bson;
using MongoDB.Driver;
using ManagementPage.Models;
using MongoDB.Bson.Serialization;

namespace ManagementPage.Database
{
    public class MongoDbClient
    {
        private const string _collectionName = "Main";
        private const string _accountsCollectionName = "Accounts";
        private const string _deviceCollectionName = "Devices";
        private const string _databaseName = "Environment";
        private readonly string _uri;

        private readonly IMongoDatabase _database;
        private readonly MongoClient _client;
        public IMongoCollection<EnvironmentData> Collection {  get; private set; }
        public IMongoCollection<UserData> AccountsCollection { get; private set; }

        public IMongoCollection<DeviceData> DeviceCollection { get; private set; }

        public MongoDbClient(IConfiguration configRoot)
        {
            _uri = configRoot["mongodb:uri"];
            _client = new MongoClient(_uri);
            _database = _client.GetDatabase(_databaseName);
            BsonClassMap.RegisterClassMap<EnvironmentData>(cm =>
            {
                cm.AutoMap();
            });

            Collection = _database.GetCollection<EnvironmentData>(_collectionName);
            AccountsCollection = _database.GetCollection<UserData>(_accountsCollectionName);
            DeviceCollection = _database.GetCollection<DeviceData>(_deviceCollectionName);
        }
    }
}
