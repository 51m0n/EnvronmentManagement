﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ManagementPage.Models
{
    public class DeviceData
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public string Name { get; set; }

        public string Passcode { get; set; }

        public Int64 ApiID { get; set; }
 
        [BsonConstructor]
        public DeviceData(string name, string passcode, Int64 apiID)
        {
            Name = name;
            Passcode = passcode;
            ApiID = apiID;
        }
    }
}
