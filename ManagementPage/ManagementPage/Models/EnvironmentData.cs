﻿using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ManagementPage.Models
{
    public class EnvironmentData
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [JsonPropertyName("apiID")]
        public Int64 ApiID { get; set; }

        [JsonPropertyName("h")]
        public double Humidity { get; set; }
        [JsonPropertyName("t")]
        public double Temperature { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Time { get; set; }

        [BsonConstructor]
        public EnvironmentData(Int64 apiId, double humidity, double temperature, DateTime time)
        {
            ApiID = apiId;
            Humidity = humidity;
            Temperature = temperature;
            Time = time;
        }
    }
}