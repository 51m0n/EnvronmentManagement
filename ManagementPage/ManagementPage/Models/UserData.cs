﻿using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ManagementPage.Models
{
    public class UserData
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [JsonPropertyName("openid")]
        public string OpenId { get; set; }

        [JsonPropertyName("localid")]
        public ObjectId LocalId { get; set; }

        [JsonPropertyName("devices")]
        public List<ObjectId> Devices { get; set; }

        [BsonConstructor]
        public UserData(string openId, ObjectId localId, List<ObjectId> devices)
        {
            OpenId = openId;
            LocalId = localId;
            Devices = devices;
        }
    }
}