﻿
function CreateChart(ctx, temp, humidity) {

    const data = {
        datasets: [
            {
                label: "Temperature",
                fill: false,
                borderColor: 'rgb(255, 55, 55)',
                data: temp,
                tension: 0.1
            },
            {
                label: "Humidity",
                fill: false,
                borderColor: 'rgb(55, 55, 255)',
                data: humidity,
                tension: 0.1,
                yAxisID: "humid"                               
            }
        ]
    };

    const config = {
        type: 'line',
        data: data,
        options: {
            maintainAspectRatio: true,
            scales: {
                y: {
                    title: {
                        display: true,
                        text: "Temperature",
                    },
                    ticks: {
                        callback: function (value, index, values) {
                            return value + '°C';
                        },
                        precision: 1,
                    }
                },
                humid: {
                    title: {
                        display: true,
                        text: "Humidity",
                    },
                    position: "right",
                    ticks: {
                        callback: function (value, index, values) {
                            return value + '%';
                        },
                        precision: 0,
                    },
                    grid: {
                        drawOnChartArea: false,
                    },
                },
                x: {
                    title: {
                        display: true,
                        text: "Time",
                    },
                    type: 'time',
                    time: {
                        unit: 'minute',
                        displayFormats: {
                            minute: "D MMM  HH:mm"
                        },
                    },
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 25
                    }
                }
            },
            plugins: {
                legend: {
                    labels: {
                        font: {
                            family: "'Quicksand', serif"
                        }
                    }
                }
            }
        }
    };

    return tempChart = new Chart(ctx, config);
}